import 'package:dog_app/dog_app_widget.dart';
import 'package:dog_app/dog_service.dart';
import 'package:flutter/material.dart';
import 'dog.dart';

void main() {
  runApp(MaterialApp(
    title: 'DogApp',
    home: DogApp(),
  ));
}


